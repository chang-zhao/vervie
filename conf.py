#############
# Main Window

# It's an `entry` widget.
# Clicking or drag-dropping there opens the popup to interact with.

# Entry width:
w = 80  # = minimal how it gets

# Set height automatically unless this is uncommented:
h = 52

# Position on screen:
xpos = 96
ypos = 1200 - h

##############
# Popup Window

# Panel placement can be: U, D, R, L, UL, DL, RU, LU
# "U" means popup will open in the "Upper" position, above the text entry
# "UL" means above the text entry and extending to the left
# (it's useful when the entry is in the bottom-right corner of the screen).
# Likewise, "D" is useful for top-left corner; "DL" for top-right corner...
popat = "U" #"RU"

# E.g. 3 rows, 4 columns
#columns = 4
#columns = 8
columns = 6

# iconSize (typically: 32 / 48 / 64 / 96 / 128 / 256)
iconSize = 64

# Shown on all desktop workplaces?
sticky = True

# Actions:
#
# Key must correspond with *.png file in ./icons
# Example:
#   "info": means there is "./icons/info.png"
# Value[0] (+ text) is the command to run by clicking that button
# Value[1] is help/tooltip string
#
buttons = {
        "wp":       ("exo-open --launch WebBrowser 'https://en.wikipedia.org/wiki/{}'",
                    "Search Wikipedia"),

        "wp-ru":    ("exo-open --launch WebBrowser 'https://ru.wikipedia.org/wiki/{}'",
                    "Википедия"),

        "wiktionary": ("exo-open --launch WebBrowser 'https://ru.wiktionary.org/wiki/{}'",
                    "Ru Wiktionary"),

        "trans":     ("exo-open --launch WebBrowser https://www.multitran.com/m.exe?s={}",
                    "Translate"),

        "process":  ("vervie process",
                    "Process text"),

        "clear":     ("vervie clear",
                    "Clear"),


        "www":      ("exo-open --launch WebBrowser '{}'",
                    "Open URL with firefox"),

        "wiki":     ("exo-open --launch WebBrowser 'http://dokuwiki/{}'",
                    "Open page in the local DokuWiki"),

        "arch": ("exo-open --launch WebBrowser 'https://wiki.archlinux.org/title/{}'",
                    "Search Arch Wiki"),

        "pkgbrowser": ("./scripts/pkgs.sh {}",
                    "Info about a package"),

        "uninstall": ("./scripts/uninstall-both.sh {}",
                    "Uninstall package(s)"),

        "pacman":   ("./scripts/install-both.sh {}",
                    "Install package(s)"),


        "www-search": ("exo-open --launch WebBrowser 'https://duckduckgo.com/?q={}'",
                    "Search web with DuckDuckGo"),

        "wiki-search": ("exo-open --launch WebBrowser 'http://dokuwiki/?do=search\&q={}'",
                    "Search local DokuWiki"),

        "cheat-sh": ("exo-open --launch WebBrowser 'http://cht.sh/{}'",
                    "Search Cheat Sheets"),

        "dl":       ("./scripts/dl_arg_v_socks.sh '{}'",
                    "Download via SOCKS5"),

        "run":      ("./scripts/terminal-cmd.sh {}",
                    "Run this in terminal"),

        "open":     ("./scripts/thunar.sh {}",
                    "Open it in File Manager"),

        }
