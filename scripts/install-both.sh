#!/bin/sh
#
# Install AUR packages with yay, other packages with pacman
#
[ -z "$1" ] && echo 'Error: Use this script with a package name!' && exit 1
THISWD=${0%/*}
pacman -Si "$1" &>/dev/null && $THISWD'/install.sh' "$1" || $THISWD'/install-yay.sh' "$1"
