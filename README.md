# vervie

Text entry with popup control panel.[^1]

Useful as an addition for desktop panels (e.g. in XFCE), or as a standalone desktop panel.

The main advantages are:

* The usual launcher buttons don't accept drag-dropping arbitrary text (only files and URIs). This tool accepts arbitrary text (as well as drag-dropping files and URIs).
* It's a convenient popup panel with as many launcher buttons as you want. You can set their icons, tooltips, commands, positions and styles (separately from the main desktop panel positions and styles).

![vervie-0.4-pic.png](/doc/vervie-0.4-pic.png)

This version has buttons:

* Search the text in English Wikipedia
* Search the text in Russian Wikipedia
* Search the text in Russian Wiktionary
* Search the text in Multitran
* Process text (evaluate) with Python
* Clear the text and close the popup
* Open URL in browser
* Open that page in DokuWiki (supposedly installed locally)
* Search the text in Arch Wiki
* Search package in PkgBrowser (it's for Arch Linux)
* Uninstall package
* Install package
* Search DuckDuckGo
* Search DokuWiki (supposedly installed locally)
* Search cht.sh ("Cheat Sheets")
* Download with SOCKS5 (should be configured)
* Run in terminal
* Open with file manager

[^1]: For Linux (Xorg/x11 kind of desktop). Tested in Arch + XFCE.

## Info

With this program you can type, paste or drag-drop text (also drag-drop files or HTML links) into the entry field. That would open the popup panel to let you click buttons to perform commands with that text as the parameter.

See how it looks like in XFCE:

Inactive vervie is just a little rectangle of the text entry, out of the way. When you click it or put some text in it, the panel opens:

![vervie-anim.gif](/doc/vervie-anim.gif)

For example, if you want to calculate some math expression, type it there and click the "Process text" button. Vervie will run Python interpreter to evaluate the expression and show you the result.

It doesn't matter if you enter text in the popup text entry or in the original (switcher) text entry; they share the same text buffer.

## Installation and options

Download the directory to whatever location you want on your computer.

    Files:

    vervie      main program, written in python + gtk3
    main.css    style (similar to ~/.config/gtk-3.0/gtk.css)
    conf.py     settings like position on screen, buttons (icon, tooltip, command)

    icons/      for buttons. You can replace them with your own, see conf.py
    scripts/    any scripts (shell, python etc.) that you might want to run with this

(1) If "vervie" does not run, set executable permission for the file. (Also maybe check the python path in the 1st line of the "vervie" file). Likewise, set executable permissions for `scripts/*` if you need them.

(2) If some dependencies are missing, install them. For example, to use Python + GTK in Arch Linux, it can be something like:

    pacman -S gtk3 gobject-introspection

(For different platforms see https://pygobject.readthedocs.io/en/latest/getting_started.html and note that you'll need gtk3, not gtk4).

(3) Edit settings in conf.py:

* Position the text entry on screen, set its size and popup positioning.
* Edit settings for buttons (icons, tooltips, commands).

(4) Put your own icons in "icons" directory, scripts in "scripts" directory. (Or soft-link them from elsewhere). (*Most scripts and icons mentioned in conf.py are not included in this distributive, so set your own commands as you please.*)

If `thunar.sh` doesn't run Thunar file manager, check its path and edit `thunar.sh` accordingly (perhaps set `/usr/bin/thunar` or just `thunar` instead of `/usr/local/bin/thunar`).

If `terminal-cmd.sh` doesn't run the terminal, then maybe your desktop is not xfce; edit `terminal-cmd.sh` accordingly (replace `xfce4-terminal` with whatever terminal command you use).

(5) When you are satisfied with the result, perhaps set "vervie" to execute in Autostart.
